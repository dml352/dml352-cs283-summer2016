#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <dirent.h>
#include <fcntl.h>
#include <time.h>
#include <sys/stat.h>
#include <sys/types.h>

const int WORD_SIZE = 128;

//delete a directory
void deleteDir(char *directory)
{
	DIR *dir = opendir(directory);
	struct dirent *run;
	while ((run = readdir(dir)) != NULL)
	{
		if (strcmp(run -> d_name, ".") != 0 && strcmp(run -> d_name, "..") != 0)
		{
			char *current = (char*) malloc(WORD_SIZE * sizeof(char));
			strcpy(current, directory);			
			strcat(current, "/");
			strcat(current, run -> d_name);
			struct stat *fileStat = (struct stat*) malloc(sizeof(struct stat));;
			stat(current, fileStat);
			if (S_ISDIR(fileStat -> st_mode))	//a directory
			{
				deleteDir(current);
				printf("Delete folder: %s\n", current);
				rmdir(current);
			}
			else		//just a regular file
			{
				printf("Delete file: %s\n", current);
				remove(current);
			}
			free(current);
			free(fileStat);
		}
	}
	closedir(dir);
	printf("Delete folder: %s\n", directory);
	rmdir(directory);
}

//copy a file from source to target
void copyFile(char *source, char *relativePath, char *target)
{
	char *sourceFile = (char*) malloc(WORD_SIZE * sizeof(char));
	strcpy(sourceFile, source);
	strcat(sourceFile, relativePath);
	char *targetFile = (char*) malloc(WORD_SIZE * sizeof(char));
	strcpy(targetFile, target);
	strcat(targetFile, relativePath);
	printf("Create and copy content from %s to %s\n", sourceFile, targetFile);
	FILE *writeFile = fopen(targetFile, "w");
	FILE *readFile = fopen(sourceFile, "r");
	char buff[WORD_SIZE];
	while(fgets(buff, WORD_SIZE, readFile) != NULL)
		fprintf(writeFile, "%s", buff);
	fclose(writeFile);
	fclose(readFile);
	free(sourceFile);
	free(targetFile);	
}

//copy a folder from source to target
void copyDir(char *source, char *relativePath, char *target)
{
	//first, create the folder on the target 
	char *targetDir = (char*) malloc(WORD_SIZE * sizeof(char));
	strcpy(targetDir, target);
	strcat(targetDir, relativePath);
	printf("Create folder: %s\n", targetDir);
	mkdir(targetDir, 0700);
	
	//now, copy everything from source folder to target folder
	char *sourceDir = (char*) malloc(WORD_SIZE * sizeof(char));
	strcpy(sourceDir, source);
	strcat(sourceDir, relativePath);
	DIR *sDir = opendir(sourceDir);
	struct dirent *dp;
	while((dp = readdir(sDir)) != NULL)
	{
		if (strcmp(dp -> d_name, ".") != 0 && strcmp(dp -> d_name, "..") != 0)
		{
			char *current = (char*) malloc(WORD_SIZE * sizeof(char));
			strcpy(current, sourceDir);
			strcat(current, "/");
			strcat(current, dp -> d_name);
			
			char *relTemp = (char*) malloc(WORD_SIZE * sizeof(char));
			strcpy(relTemp, relativePath);
			strcat(relTemp, "/");
			strcat(relTemp, dp -> d_name);
				
			struct stat *fileStat = (struct stat*) malloc(sizeof(struct stat));
			stat(current, fileStat);
			if (S_ISDIR(fileStat -> st_mode)) //a directory
			{
				printf("Copy folder from %s%s to %s%s\n", source, relTemp, target, relTemp);
				copyDir(source, relTemp, target);
			}
			else 	//a regular file
				copyFile(source, relTemp, target);
			free(relTemp);
			free(current);
			free(fileStat);
		}
	}
	closedir(sDir);
	free(sourceDir);
	free(targetDir);
}

//trimming the target folder
void trim(char *target, char *relativePath, char *source)
{
	DIR *targetDIR;
	if (strcmp(relativePath, "") == 0)
	{
		targetDIR = opendir(target);
	}
	else
	{
		char *temp = (char*) malloc(WORD_SIZE * sizeof(char));
		strcpy(temp, target);
		strcat(temp, relativePath);
		targetDIR = opendir(temp);
		free(temp);
	}
	struct dirent *run;
	while ((run = readdir(targetDIR)) != NULL)
	{
		if (strcmp(run -> d_name, ".") != 0 && strcmp(run -> d_name, "..") != 0)
		{
			char *temp = (char*) malloc(WORD_SIZE * sizeof(char));
			strcpy(temp, target);
			strcat(temp, relativePath);
			strcat(temp, "/");
			strcat(temp, run -> d_name);
			
			struct stat *fileStat = (struct stat*) malloc(sizeof(struct stat));
			stat(temp, fileStat);
			
			if (S_ISDIR(fileStat -> st_mode))	//a directory, we look into it now
			{
				//check if this directory exists under source. If not, we delete everything in it
				char *tempSource = (char*) malloc(WORD_SIZE * sizeof(char));
				strcpy(tempSource, source);
				if (strcmp(relativePath, "") != 0)
				{
					strcat(tempSource, "/");
					strcat(tempSource, relativePath);
				}
				
				strcat(tempSource, "/");
				strcat(tempSource, run -> d_name);
				
				DIR *look;
				look = opendir(tempSource);
				
				if (look == NULL)	//the folder does not exist in source. we delete it in target now
				{
					char *delTarget = (char*) malloc(WORD_SIZE * sizeof(char));
					strcpy(delTarget, target);
					if (strcmp(relativePath, "") != 0)
						strcat(delTarget, relativePath);
					strcat(delTarget, "/");
					strcat(delTarget, run -> d_name);
					deleteDir(delTarget);
					free(delTarget);
				}
				else
				{
					//we have a same-name directory in source,
					char *relTemp = (char*) malloc(WORD_SIZE * sizeof(char));
					strcpy(relTemp, relativePath);
					strcat(relTemp, "/");
					strcat(relTemp, run -> d_name);
					trim(target, relTemp, source);
					free(relTemp);
				}			
				free(tempSource);
				closedir(look);
			}
			
			else	//a regular file
			{
				char *compare = (char*) malloc(WORD_SIZE * sizeof(char));
				strcpy(compare, source);
				strcat(compare, relativePath);
				strcat(compare, "/");
				strcat(compare, run -> d_name);
				int check = open(compare, O_RDWR);
				if (check < 0) 		//this file is not in source, delete it
				{
					printf("Delete file: %s\n", temp);
					remove(temp);
					close(check);
				}
				else
				{
					//this file is also in source, now update both files to the newer version
					close(check);
					time_t targetTime = fileStat -> st_mtime;
					struct stat *sourceStat = (struct stat*) malloc(sizeof(struct stat));
					stat(compare, sourceStat);
					time_t sourceTime = sourceStat -> st_mtime;
					int readFD, writeFD;
					if (difftime(targetTime, sourceTime) < 0)	//targetFile is modified earlier than sourceFile, read from sourceFile, and write to targetFile
					{
						readFD = open(compare, O_RDONLY);
						writeFD = open(temp, O_WRONLY | O_TRUNC);
					}
					else // if (difftime(targetTime, sourceTime) > 0)
					{
						readFD = open(temp, O_RDONLY);
						writeFD = open(compare, O_WRONLY | O_TRUNC);
					}
					printf("Update files %s and %s to the newer version of the 2\n", compare, temp);
					char *token = (char*) malloc(WORD_SIZE * sizeof(char));
					int len;
					while((len = read(readFD, token, 1)) == 1)
						write(writeFD, token, 1);
					close(readFD);
					close(writeFD);
					free(token);
					free(sourceStat);
				}
				free(compare);
			}
			free(temp);
			free(fileStat);
		}
	}
	closedir(targetDIR);
}

//fillIn the target folder with missing files
void fillIn(char *source, char *relativePath, char *target)
{
	//first, check if the given path is not in target folder. IF not, we simply create entire directory in the target folder
	char *checkTarget = (char*) malloc(WORD_SIZE * sizeof(char));
	strcpy(checkTarget, target);
	strcat(checkTarget, relativePath);
	
	char *checkSource = (char*) malloc(WORD_SIZE * sizeof(char));
	strcpy(checkSource, source);
	strcat(checkSource, relativePath);
	DIR *sourceDir = opendir(checkSource);
	struct dirent *run;
	while((run = readdir(sourceDir)) != NULL)
	{
		if (strcmp(run -> d_name, ".") != 0 && strcmp(run -> d_name, "..") != 0)
		{
			char *currentSource = (char*) malloc(WORD_SIZE * sizeof(char));
			strcpy(currentSource, checkSource);
			strcat(currentSource, "/");
			strcat(currentSource, run -> d_name);
			struct stat *fileStat = (struct stat*) malloc(sizeof(struct stat));
			stat(currentSource, fileStat);
			if (S_ISDIR(fileStat -> st_mode))	//a directory
			{
				char *relTemp = (char*) malloc(WORD_SIZE * sizeof(char));
				strcpy(relTemp, relativePath);
				strcat(relTemp, "/");
				strcat(relTemp, run -> d_name);
				
				char *newFile = (char*) malloc(WORD_SIZE * sizeof(char));
				strcpy(newFile, target);
				strcat(newFile, "/");
				strcat(newFile, relTemp);
				printf("Copy dir, from %s%s to %s%s\n", source, relTemp, target, relTemp);
				copyDir(source, relTemp, target);
				free(relTemp);
				free(newFile);
			}
			else	//a regular file
			{
				char *relTemp = (char*) malloc(WORD_SIZE * sizeof(char));
				strcpy(relTemp, relativePath);
				strcat(relTemp, "/");
				strcat(relTemp, run -> d_name);
				copyFile(source, relTemp, target);
				free(relTemp);
			}
			free(currentSource);
			free(fileStat);
		}
	}
	free(checkTarget);
	free(checkSource);
	closedir(sourceDir);
}

int checkValid(char *source, char *target)
{
	if (strstr(source, target) != NULL || strstr(target, source) != NULL)	//1 string contains another
		return 1;
	return 0;
}

int main(int argc, char **argv)
{
	//source is a, target is b
	char *a = (char*) malloc(WORD_SIZE * sizeof(char));
	strcpy(a, argv[1]);
	char *b = (char*) malloc(WORD_SIZE * sizeof(char));
	strcpy(b, argv[2]);
	
	if (checkValid(a,b) != 0)
	{
		printf("%s\n", "Can't work with those 2 directories");
		return 1;
	}
	trim(b, "", a);
	fillIn(a, "", b);
	free(a);
	free(b);
}