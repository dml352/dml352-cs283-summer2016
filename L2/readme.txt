Name: Duc Le
CS283 - Summer 2016
Lab2 - File IO

The source code is in fileIO3.c. 
The program sync 2 folders in 3 steps:
	If 1 program is a subdirectory of the other, the program halts.
	Traverse b, delete all files that are not in a. Also, update files in a and b if the same file exists in both directory.
	Traverse a, create additional files in b
The Makefile has 1 required build target. The program runs correctly, but prints out "make: *** [sync] Error 1". Since it 
does not affect the required functionality, I do not fix it.
valgrind was used for testing, and no memory leaks were present.
