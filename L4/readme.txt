Name: Duc Le
dml352@drexel.edu

Make commands:
make build: prepares the files.
make test1: run test without locking
make test2: run test with locks inside the loop
make test3: run test with lock outside the loop

Observation:
Putting locks inside the loops give correct result. However, the time is very long, because we lock and unlock everytime the count is incremented.
Putting locks outside the loops give correct count, and also the fastest time. However, since we lock and unlock the resource for each thread,
this program is sequential in nature (each thread can only execute after the another one unlock).