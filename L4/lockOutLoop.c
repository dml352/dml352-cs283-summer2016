#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

volatile int count = 0;
pthread_mutex_t lock = PTHREAD_MUTEX_INITIALIZER;

void *leave(void *arg)
{
	int i;
	pthread_mutex_lock(&lock);
	for (i = 0; i < 1000; i++)
		count = count + 1;
	pthread_mutex_unlock(&lock);
	return NULL;
}

int main()
{
	int run;
	int avgclock = 0;
	for (run = 0; run < 10; run++)
	{
		count = 0;
		clock_t t1, t2;
		t1 = clock();
		pthread_t crowd[100];
		int i;
		for (i = 0; i < 100; i++)
			pthread_create(&crowd[i], NULL, leave, NULL);
		for (i = 0; i < 100; i++)
			pthread_join(crowd[i], NULL);
		t2 = clock();
		clock_t t = t2 - t1;
		avgclock = avgclock + t;
		printf("Run #%d: %d, end after: %d\n", run, count, t);
	}
	printf("Average clock (lock outside loop): %d\n", avgclock / 10);
	return 0;
}