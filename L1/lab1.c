#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct List {
	int val;
	struct List* next;
}

//source:
//http://www.programmingsimplified.com/c/source-code/c-program-bubble-sort
void sort(int *l, int size)
{
	int c;
	int d;
	int swap;
	for(c = 0; c < size - 1; c++)
	{
		for (d = 0; d < size - c - 1; d++)
		{
			if (*(l + d) > *(l + d + 1))
			{
				swap = *(l+d);
				*(l+d) = *(l+d+1);
				*(l+d+1) = swap;
			}
		}
	}
}

int main()
{
	int *p = (int*) malloc(10 * sizeof(int));
	if (p == NULL)
	{
		perror("malloc");
		exit(0);
	}
	int i;
	for (i = 0; i < 10; i++)
		p[i] = i;

	printf("%s\n", "10 integer: ");

	for (i = 0; i < 10; i++)
		printf("%d ", p[i]);
	printf("%s\n", "");

	free(p);

	char **c = (char**) malloc(10 * sizeof(char*));
	for (i = 0; i < 10; i++)
	{
		c[i] = (char*) malloc(15 * sizeof(char));
	}
	strcpy(c[0], "Hello!");
	strcpy(c[1], "How are you?");
	strcpy(c[2], "I am good.");
	strcpy(c[3], "How are you?");
	strcpy(c[4], "not bad");
	strcpy(c[5], "Did HW?");
	strcpy(c[6], "no");
	strcpy(c[7], "Did Lab?");
	strcpy(c[8], "Not yet");
	strcpy(c[9], "Don't.");
	printf("%s\n", "String print: ");
	for (i = 0; i < 10; i++)
	{
		printf("%s\n", c[i]);
	}

	for (i = 0; i < 10; i++)
	{
		free(c[i]);
	}
	free(c);

	int *sTest = (int*) malloc(4 * sizeof(int));
	sTest[0] = 4;
	sTest[1] = 2;
	sTest[2] = 3;
	sTest[3] = 1;

	printf("%s: ", "pre-sort");
	for (i = 0; i < 4; i++)
	{
		printf("%d ", sTest[i]);
	}
	printf("%s\n", "");
	sort(sTest, 4);
	printf("%s: ", "post-sort");
	for (i = 0; i < 4; i++)
	{
		printf("%d ", sTest[i]);
	}
	printf("%s\n", "");
	free(sTest);

		
	return 0;
}
