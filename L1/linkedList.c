#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct List {
	int val;
	struct List* next;
};

int size(struct List* l)
{
	if (l == NULL)
		return 0;
	return 1 + size(l -> next);
}

void printList(struct List* l)
{
	struct List* temp = l;
	while (temp != NULL)
	{
		printf("%d ", temp -> val);
		temp = temp -> next;
	}
	printf("%s\n", "");
}

struct List* swap2(struct List *l, struct List* a)	//swap a and (a+1)
{
	if (a == l)		//a is the first node
	{
		l = a -> next;
		a -> next = a -> next -> next;
		l -> next = a;
	}
	else
	{
		struct List *prea = l;
		while (prea -> next != a)
			prea = prea -> next;
		prea -> next = a -> next;
		a -> next = a -> next -> next;
		prea -> next -> next = a;
	}
	return l;
}

struct List* sort(struct List *l)
{
	int lim = size(l);
	int run1 = 0;
	while (run1 < lim)
	{
		struct List *run2 = l;
		while (run2 != NULL)
		{
			if ((run2 -> next != NULL) && (run2 -> val) > (run2 -> next -> val))
				l = swap2(l, run2);
			else
			{
				run2 = run2 -> next;
			}
		}
		run1++;
	}
	return l;
}



void add(struct List* l, int a)
{
	if (l == NULL)
	{
		l = (struct List*) malloc(sizeof(struct List*));
		l -> val = a;
		l -> next = NULL;
	}
	else
	{
		struct List* temp = l;
		while(temp -> next != NULL)
			temp = temp -> next;
		temp -> next = (struct List*) malloc(sizeof(struct List*));
		temp -> next -> val = a;
		temp -> next -> next = NULL;
	}
}

void cleanUp(struct List* l)
{
	struct List* temp;
	while (l != NULL)
	{
		temp = l;
		l = l -> next;
		free(temp);
	}	
}

int main()
{
	struct List* l = (struct List*) malloc(sizeof(struct List*));
	l -> val = 9;
	l -> next = NULL;
	add(l, 3);
	add(l, 2);
	add(l, 1);
	add(l, 4);
	printf("%s", "Pre-sort: ");
	printList(l);
	printf("%s", "Post-sort: ");
	l = sort(l);
	printList(l);
	cleanUp(l);
	return 0;
}