#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int size;
int TEST = 1000000;

int* init(int n)
{
	return (int*) malloc(n * sizeof(int));
}

int* add1(int* a, int index, int val)
{
	if(index < size)
	{
		a[index] = val;
		return a;
	}
	else
	{
		while (index >= size)
			size++;
		a = (int*) realloc(a, size * sizeof(int));
		a[index] = val;
		return a;
	}
}

int* add2(int* a, int index, int val)
{
	if(index < size)
	{
		a[index] = val;
		return a;
	}
	else
	{
		while (index >= size)
			size = size * 2;
		a = (int*) realloc(a, size * sizeof(int));
		a[index] = val;
		return a;
	}
}

int get(int *a, int index)
{
	if (index < size)
		return a[index];
	else
		return -1; 	//suppose we have an array of positive integer, and -1 means error
}

int* removeItem(int *a, int index)
{
	int run = index;
	for (run = index; run < size; run++)
		a[run] = a[run + 1];
	size--;
	return (int*) realloc(a, size * sizeof(int));
}

int main()
{
	clock_t t1 = clock();
	int *a = init(10);
	size = 10;
	int i = 0;
	for (i = 0; i < TEST; i++)
		a = add1(a, i, i);
	clock_t t2 = clock();
	printf("%s%ld\n", "Time for incrementing the size: ", t2 - t1);
	printf("%s%d\n", "Test get, item at index 100 is: ", get(a, 100));
	
	t1 = clock();
	int *b = init(10);
	size = 10;
	for (i = 0; i < TEST; i++)
		b = add2(b, i, i);
	t2 = clock();
	printf("%s%ld\n", "Time for doubling the size: ", t2 - t1);
	printf("%s\n", "Test remove, item at index 200 before and after removal: ");
	printf("%d\n", get(b, 200));
	b = removeItem(b, 200);
	printf("%d\n", get(b, 200));
	free(a);
	free(b);
	
	return 0;
}