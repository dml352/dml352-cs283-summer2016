Name: Duc Minh Le
Drexel Id: dml352
Assignment 1 - Anagram and Scrabble solver

The source code is in linkedAnagram.c. The program implements the hashtable as a linked list of linked list. The programs
reads the dictionary input from a file named dictionary5. This file was created by processing the words file ("/usr/share/dict/words").
To create dictionary5, I eliminate all quotation marks, turns all the words to lowercase, eliminate the last 17 non-English
words, sort and get the list of unique words. The code for these process is stored in convert2, a bash file. dictionary5 is included
in the submission, but it can be created by running the bash code in convert2.

The Makefile has the following command:
	getDictionary: get to "/usr/share/dict/words" and run convert2 to create the dictionary5 file.
	build: compile linkedAnagram.c
	anagram: find anagram of a given word. Example: WORD=eat make anagram
	scrabble: play scrabble for a given word. Example: WORD=eat LETTER=t PLACE=1 make scrabble
	clean: remove the a.out file.
	mTestAnagram and mTestScrabble are memory tests using valgrind. The syntax is similar to anagram and scrabble. The program has been tested and
		no memory leaks found.

Sample run: after running make build:
"WORD=eat make anagram" produces output:
	The anagrams for eat:
	tea
	ate
	eat
	eta

"WORD=eat LETTER=t PLACE=1 make scrabble" produces output:
	Play scrabble for eat:
	ate
	eta
	
The implementation uses a linked list of linked lists. This approach uses only about 25% of space compared to my previous approach (array of array). 
However, I believe the best approach is array of linked lists. An array of linked list has faster search time. Also, since English word length is not
prohibitively large (longest words do not exceed 35, except for rare exceptions), so an array of size 1000 should be enough to accomodate all hash values.

Still, working with linked list really improves my ability to use pointers. I still prefer higher level language, but there is a satisfaction in creating
a clean C program with no memory leaks.