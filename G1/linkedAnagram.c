#include <stdio.h>
#include <string.h>
#include <stdlib.h>

const int WORD_SIZE = 50;

struct wordList  {
	char *word;
	struct wordList *next;
};

struct tableList {
	int value;
	struct wordList *list;
	struct tableList *next;
};

//clean up
void freeList(struct wordList *head)
{
	struct wordList *temp;
	struct wordList *run = head;
	while(run != NULL)
	{
		temp = run;
		run = run -> next;
		free(temp -> word);
		free(temp);
	}
}

void freeTable(struct tableList *head)
{
	struct tableList *temp;
	struct tableList *run = head;
	while(run != NULL)
	{
		temp = run;
		run = run -> next;
		freeList(temp -> list);
		free(temp);
	}
}

int hashChar(char c)
{
	//get the ascii code
	int ascii = (int) (c);
	if (ascii > 64 && ascii < 91)	//uppercase character
		return (ascii - 64);
	else if (ascii > 96 && ascii < 123)		//lowercase character
		return (ascii - 96);
	else 							//all other character hash to 0
		return 0;
}

int hashWord(const char *s)
{
	int i;
	int map = 0;
	int size = strlen(s);
	for (i = 0; i < size; i++)
	{
		map += hashChar(s[i]);
	}
	return map;
}

int isAnagram(char *first, char *second)
{
	int count1[27];
	int count2[27];
	int i = 0;
	for (i = 0; i < 27; i++)
	{
		count1[i] = 0;
		count2[i] = 0;
	}
	
	i = 0;
	while (first[i] != '\0')
	{
		count1[hashChar(first[i])]++;
		i++;
	}
	
	i = 0;
	while (second[i] != '\0')
	{
		count2[hashChar(second[i])]++;
		i++;
	}
	
	for (i = 0; i < 27; i++)
	{
		if (count1[i] != count2[i])
			return -1;
	}
	return 0;
}

void insertWordToTable(char *word, struct tableList *hashTable)
{
	int val = hashWord(word);
	struct tableList *pre = hashTable;
	while (pre -> next != NULL && pre -> next -> value < val)
		pre = pre -> next;
	if (pre -> next == NULL)	//new hash value, that is the biggest hash value yet
	{
		struct tableList *newList = (struct tableList*) malloc(sizeof(struct tableList));
		newList -> value = val;
		newList -> next = NULL;
		struct wordList *l = (struct wordList*) malloc(sizeof(struct wordList));
		l -> next = NULL;
		l -> word = (char*) malloc(WORD_SIZE * sizeof(char));
		strcpy(l -> word, word);
		newList -> list = l;
		pre -> next = newList;
	}
	else if (pre -> next -> value == val)	//existing hash value
	{
		pre = pre -> next;
		struct wordList *newNode = (struct wordList*) malloc(sizeof(struct wordList));
		newNode -> next = pre -> list;
		newNode -> word = (char*) malloc(WORD_SIZE * sizeof(char));
		strcpy(newNode -> word, word);
		pre -> list = newNode;		
	}
	else	//new hash value, 
	{
		struct tableList *newList = (struct tableList*) malloc(sizeof(struct tableList));
		newList -> value = val;
		newList -> next = pre -> next;
		struct wordList *l = (struct wordList*) malloc(sizeof(struct wordList));
		l -> next = NULL;
		l -> word = (char*) malloc(WORD_SIZE * sizeof(char));
		strcpy(l -> word, word);
		newList -> list = l;
		pre -> next = newList;
	}
}

struct wordList* insertWordToList(char *word, struct wordList *list)
{
	if (list == NULL)	//empty list
	{
		list = (struct wordList*) malloc(sizeof(struct wordList));
		list -> next = NULL;
		list -> word = (char*) malloc(WORD_SIZE * sizeof(char));
		strcpy(list -> word, word);
	}
	else
	{
		struct wordList *newNode = (struct wordList*) malloc(sizeof(struct wordList));
		newNode -> next = list -> next;
		newNode -> word = (char*) malloc(WORD_SIZE * sizeof(char));
		strcpy(newNode -> word, word);
		list -> next = newNode;
	}
	return list;
}

struct wordList* findAnagram(char *word, struct tableList *hashTable)
{
	struct wordList *res = NULL;
	int hash = hashWord(word);
	struct tableList *run = hashTable;
	while (run != NULL && run -> value < hash)
		run = run -> next;
	if (run != NULL)
	{
		struct wordList *l = run -> list;
		while (l != NULL)
		{
			if (isAnagram(l -> word, word) == 0)
				res = insertWordToList(l -> word, res);
			l = l -> next;
		}
	}
	return res;
}

struct wordList* playScrabble(char *word, struct tableList *hashTable, char letter, int index)
{
	struct wordList *anagram = findAnagram(word, hashTable);
	struct wordList *res = NULL;
	struct wordList *temp = anagram;
	while (temp != NULL)
	{
		if ((temp -> word)[index] == letter)
			res = insertWordToList(temp -> word, res);
		temp = temp -> next;
	}
	freeList(anagram);
	return res;
}

int main(int argc, char **argv) {
	struct tableList *hashTable = (struct tableList*) malloc(sizeof(struct tableList));
	hashTable -> value = 0;
	hashTable -> list = NULL;
	hashTable -> next = NULL;
		
	FILE* dict = fopen("dictionary5", "r"); //open the dictionary for read-only access
    if(dict == NULL) {
		printf("%s\n", "Error finding file");
        return -1;
    }

	
    // Read each line of the file, and print it to screen
    char word[128];
    while(fgets(word, sizeof(word), dict) != NULL) {
		int len = strlen(word);
		word[len-1] = '\0';
		insertWordToTable(word, hashTable);
    }
	
	if (argc == 2)
	{
		char *word = argv[1];
		struct wordList *res = findAnagram(word, hashTable);
		printf("%s%s: \n", "Anagrams for ", word);
		struct wordList *temp = res;
		while (temp != NULL)
		{
			printf("%s\n", temp -> word);
			temp = temp -> next;
		}
		freeList(res);
	}
	else if (argc == 4)
	{
		char *word = argv[1];
		char letter = argv[2][0];
		int index = atoi(argv[3]);
		struct wordList *res = playScrabble(word, hashTable, letter, index);
		printf("%s%s: \n", "Play scrabble for ", word);
		struct wordList *temp = res;
		while (temp != NULL)
		{
			printf("%s\n", temp -> word);
			temp = temp -> next;
		}
		freeList(res);
	}
	freeTable(hashTable);
	fclose(dict);
	return 0;
}

