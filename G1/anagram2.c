#include <stdio.h>
#include <string.h>
#include <stdlib.h>


const int HASH_TABLE_SIZE = 650;
const int STRING_MAX_SIZE = 100;
const int START_LIST_SIZE = 10;
const int LIST_SIZE = 1000;
const int LIST_GROW_FACTOR = 2;

struct List  {
	char **wordList;
	int size;
};

//clean up memory
void freeList(struct List *list)
{
	int i = 0;
	/**
	while ((list -> wordList)[i] != NULL)
	{
		free((list -> wordList[i]));
		i++;
	}**/
	for (i = 0; i < LIST_SIZE; i++)
		free((list -> wordList[i]));
	free(list -> wordList);
}

void freeTable(struct List *hashTable)
{
	int i;
	for (i = 0; i < HASH_TABLE_SIZE; i++)
		freeList(&(hashTable[i]));
	free(hashTable);
}

int hashChar(char c)
{
	//get the ascii code
	int ascii = (int) (c);
	//printf("%c %d\n", c, ascii);
	if (ascii > 64 && ascii < 91)	//uppercase character
		return (ascii - 64);
	else if (ascii > 96 && ascii < 123)		//lowercase character
		return (ascii - 96);
	else 
		return 0;
}

int hashWord(const char *s)
{
	int i;
	int map = 0;
	int size = strlen(s);
	for (i = 0; i < size; i++)
	{
		map += hashChar(s[i]);
	}
	return map;
}

void insertWordToList(char *word, struct List *list)
{
	int i = 0;
	while((list -> wordList)[i] != NULL)
	{
		//printf("%s\n", (list -> wordList)[i]);
		i++;
	}
	(list -> wordList)[i] = (char*) malloc(STRING_MAX_SIZE * sizeof (char));
	strcpy((list -> wordList)[i], word);
}

int isAnagram(char *first, char *second)
{
	int count1[27];
	int count2[27];
	int i = 0;
	for (i = 0; i < 27; i++)
	{
		count1[i] = 0;
		count2[i] = 0;
	}
	
	i = 0;
	while (first[i] != '\0')
	{
		count1[hashChar(first[i])]++;
		i++;
	}
	
	i = 0;
	while (second[i] != '\0')
	{
		count2[hashChar(second[i])]++;
		i++;
	}
	
	for (i = 0; i < 27; i++)
	{
		if (count1[i] != count2[i])
			return -1;
	}
	return 0;
}

void insertWordToTable(char *word, struct List *hashTable)
{
	int loc = hashWord(word);
	insertWordToList(word, (struct List*) &hashTable[loc]);
}

struct List findAnagram(char *word, struct List *hashTable)
{
	struct List res;
	res.wordList = (char**) malloc(LIST_SIZE * sizeof(char*));
	int resRun = 0;
	struct List lookUp = hashTable[hashWord(word)];
	int lookRun = 0;
	while ((lookUp.wordList)[lookRun] != NULL)
	{
		if (isAnagram(word, (lookUp.wordList)[lookRun]) == 0)
		{
			res.wordList[resRun] = (char*) malloc(STRING_MAX_SIZE * sizeof(char));
			strcpy(res.wordList[resRun], (lookUp.wordList)[lookRun]);
			resRun++;
		}
		lookRun++;
	}
	return res;
}

struct List playScrabble(char *word, struct List *hashTable, char letter, int index)
{
	struct List res;
	res.wordList = (char**) malloc(LIST_SIZE * sizeof(char*));
	int resRun = 0;
	struct List anagram = findAnagram(word, hashTable);
	int anRun = 0;
	while ((anagram.wordList)[anRun] != NULL)
	{
		if ((anagram.wordList)[anRun][index] == letter)
		{
			res.wordList[resRun] = (char*) malloc(STRING_MAX_SIZE * sizeof(char));
			strcpy(res.wordList[resRun], (anagram.wordList)[anRun]);
			resRun++;
		}
		anRun++;
	}
	freeList(&anagram);
	return res;
}

int main(int argc, char **argv)
{ 
	struct List *hashTable = (struct List*) malloc(HASH_TABLE_SIZE * sizeof(struct List));
	int i = 0;
	for (i = 0; i < HASH_TABLE_SIZE; i++)
		hashTable[i].wordList = (char**) malloc(LIST_SIZE * sizeof(char*));
	FILE* dict = fopen("dictionary5", "r"); //open the dictionary for read-only access
    if(dict == NULL) {
		printf("%s\n", "Error finding file");
        return -1;
    }

	
    // Read each line of the file, and print it to screen
    char word[128];
    while(fgets(word, sizeof(word), dict) != NULL) {
		int len = strlen(word);
		word[len-1] = '\0';
		insertWordToTable(word, hashTable);
    }
	
	if (argc == 2)
	{
		char *word = argv[1];
		printf("%s\n", word);
		struct List res = findAnagram(word, hashTable);
		i = 0;
		printf("%s%s: \n", "Anagrams for ", word);
		while ((res.wordList)[i] != NULL)
		{
			printf("%s\n", (res.wordList)[i]);
			i++;
		}
		free(word);
		freeList(&res);
	}
	else if (argc == 4)
	{
		char *word = argv[1];
		char letter = argv[2][0];
		int index = atoi(argv[3]);
		struct List res = playScrabble(word, hashTable, letter, index);
		int i = 0;
		printf("%s%s: \n", "Play Scrabble for ", word);
		while ((res.wordList)[i] != NULL)
		{
			printf("%s\n", (res.wordList)[i]);
			i++;
		}
		free(word);
		freeList(&res);
	}
	freeTable(hashTable);
	free(hashTable);
	free(dict);
	return 0;
}