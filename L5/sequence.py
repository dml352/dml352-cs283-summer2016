import sys

header = ''

def getValByKey(row, col):
    global header
    result = ''
    idx = 0

    for keycol in header.split(','):
        if keycol == col:
            result = row.split(',')[idx]
            break
        idx = idx + 1
   
    result = result.strip().replace('\'', '')
    return result

def main():
    global header
    result = dict()
    process_data = []
    file = sys.argv[1]
    csvfile = open(file, 'r')
    lineno = 0

    for line in csvfile:
       if lineno > 0:
            process_data.append(line)
       else:
            header = line
       lineno = lineno + 1

    for row in process_data:
        train = getValByKey(row, 'train_id')
        if not (train in result):
            result[train] = dict()
        else: 
            station = getValByKey(row, 'next_station')
            if not (station in result[train]):
                result[train][station] = []
            else:
                status = getValByKey(row, 'status')
                if (status == 'On Time' or status == '1 min' or status == '2 min' 
                  or status == '3 min' or status == '4 min' or status == '5 min'):
                    result[train][station].append(1)
                else:
                    result[train][station].append(0)
    
    res = dict()
    for train in result:
        if not (train in res):
            res[train] = dict()
        for station in result[train]:
            record = result[train][station]
            total = len(record)
            onTime = 0
            for i in record:
                onTime = onTime + i
            if total == 0:
                res[train][station] = 0
            else:
                res[train][station] = 100 * float(onTime) / float(total)

    for train in res:
        for station in res[train]:
            print "Train: ", train, ", station: ", station, ", ", res[train][station], "%"   

if __name__ == "__main__":
    main()