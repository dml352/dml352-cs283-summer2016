import sys
from multiprocessing import Pool 

def getValByKey(row, col):
    global header
    result = ''
    idx = 0

    for keycol in header.split(','):
        if keycol == col:
            result = row.split(',')[idx]
            break
        idx = idx + 1
   
    result = result.strip().replace('\'', '')
    return result

'''
mapper function
create a dictionary for all train id
for each train id, create a dictionary for each station
each station record is then mapped to a list of 1 (on time) and 0 (not on time)
'''

def mapper(process_data):
    result = dict()
    for row in process_data:
        train = getValByKey(row, 'train_id')
        if not (train in result):
            result[train] = dict()
        else: 
            station = getValByKey(row, 'next_station')
            if not (station in result[train]):
                result[train][station] = []
            else:
                status = getValByKey(row, 'status')
                if (status == 'On Time' or status == '1 min' or status == '2 min' 
                  or status == '3 min' or status == '4 min' or status == '5 min'):
                    result[train][station].append(1)
                else:
                    result[train][station].append(0)
    return result

'''
partition function
we have an array of train -> station -> on time list
now we aggregate them, so that each train has its own records of stations and on-time 
'''
def partition(mappings_list):
    result = dict()
    for mapping in mappings_list:
        for train in mapping:
            if not (train in result):
                result[train] = dict()
            for station in mapping[train]:
                if not (station in result[train]):
                    result[train][station] = []
                result[train][station].extend(mapping[train][station])
    return result

'''
reducer function
'''    
def reducer(tup):
    result = dict()
    train = tup[0]
    if not (train in result):
        result[train] = dict()
    for station in tup[1]:
        record = tup[1][station]
        total = len(record)
        onTime = 0
        for i in record:
            onTime = onTime + i
        if total == 0:
            result[train][station] = 0
        else:
            result[train][station] = 100 * float(onTime) / float(total)
    return result

header = ''
def main():
    global header
    file = sys.argv[1]
    processes = int(sys.argv[2])

    csvlines = []
    csvfile = open(file, 'r')
    lineno = 0

    for line in csvfile:
       if lineno > 0:
            csvlines.append(line)
       else:
            header = line
       lineno = lineno + 1
    numlines = len(csvlines)
    lines_per_process = numlines / processes

    process_data_array = []
    for i in range(processes):
        start = i * lines_per_process
        end = (i + 1) * lines_per_process
        process_data_array.append(csvlines[int(start):int(end)])
    
    pool = Pool(processes = processes)

    mapping = pool.map(mapper, process_data_array)
    shuffled = partition(mapping)
    reduced = pool.map(reducer, shuffled.items())

    
    for i in range(len(reduced)):
        for train in reduced[i]:
            for station in reduced[i][train]:
                print "Train: ", train, ", station: ", station, ", ", reduced[i][train][station], "%"
    
if __name__ == "__main__":
    main()

