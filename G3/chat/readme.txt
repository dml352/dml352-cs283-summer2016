Name: Duc Le
Email: dml352@drexel.edu

The Makefile has the necessary targets. All targets work well.
NetworkRead and NetworkWrite are thread classes that do the writing and reading.

make build will prepares the files.

To test the program, runs genkey twice to get 2 pairs of keys. Example: 
dml352@tux64-14 chat2> make genkey NPRIME=17 MPRIME=13
java KeyGenTest 13 17
Public key (385, 221), private key (1, 221)
dml352@tux64-14 chat2> make genkey NPRIME=11 MPRIME=19
java KeyGenTest 19 11
Public key (371, 209), private key (131, 209)

Then, open a server:
dml352@tux64-14 chat2> make server PORT=1999 E=385 C=221 D=131 DC=209
java Server 1999 385 221 131 209
Waiting for client to connect to port: 1999...

And a client:
dml352@tux64-14 chat2> make client SERVER=tux64-14.cs.drexel.edu PORT=1999 E=371 C=209 D=1 DC=221
java Client tux64-14.cs.drexel.edu 1999 371 209 1 221

Remember to connect to the correct server.

Then, the chat program is running. You can keep sending messages. When you want to quit, just type quit, the 2 programs will end.

make keycrack is also working. The program will figure out the original 2 primes, and do encryption and decryption with them. Example:
dml352@tux64-14 chat2> make keycrack E=581 C=323
java CrackTest 581 323
a: 17, b: 19, e: 581, m: 288
173
Encrypt 'a': 241
Decrypt back to: a


