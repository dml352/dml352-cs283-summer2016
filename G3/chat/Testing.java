package chat;

import java.io.IOException;

public class Testing {
	public static void main(String[] args)
	{
		try
		{
			Server server = new Server(Integer.parseInt(args[0]));
			server.setKey(Long.parseLong(args[1]), Long.parseLong(args[2]), Long.parseLong(args[3]), Long.parseLong(args[4]));
			server.start();
			
			Client client = new Client("localhost", Integer.parseInt(args[0]));
			client.setKey(Long.parseLong(args[1]), Long.parseLong(args[2]), Long.parseLong(args[3]), Long.parseLong(args[4]));
			client.start();			
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
	}
}
