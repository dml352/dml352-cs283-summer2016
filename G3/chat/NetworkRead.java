//package chat;

import java.io.*;

// read client's message and print to screen
//source: http://www.tutorialspoint.com/java/java_networking.htm

public class NetworkRead extends Thread {
	private DataInputStream in;
	long e, c, d, dc;

	public NetworkRead(DataInputStream i)
	{
		in = i;
	}
	
	public void setKey(long first, long second, long third, long fourth)
	{
		e = first;
		c = second;
		d = third;
		dc = fourth;
	}
	
	public void run()
	{
		String buf = "";
		try
		{
			while(!buf.equals("quit"))
			{
				String mes = "Guess: ";
				char debuf = '0';
				while(debuf != '\n')
				{
					buf = in.readUTF();
					if (buf.equals("quit"))
					{
						System.out.println("Guess quits.");
						System.exit(0);
					}
					debuf = (char) MiniRSA.decrypt(Long.parseLong(buf), d, dc);
					mes = mes + debuf;
				}
				System.out.print(mes);
			}
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
	}
}
