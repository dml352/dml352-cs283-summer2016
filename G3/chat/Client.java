//package chat;

import java.net.*;
import java.io.*;

public class Client extends Thread {
	long e, c, d, dc;
	String host;
	int port;
	
	/**String host = args[0];
		int port = Integer.parseInt(args[1]);
		setKey(Long.parseLong(args[2]), Long.parseLong(args[3]), Long.parseLong(args[4]), Long.parseLong(args[5]));
	 */
	public void setKey(long first, long second, long third, long fourth)
	{
		e = first;
		c = second;
		d = third;
		dc = fourth;
	}
	
	public Client(String h, int p)
	{
		host = h;
		port = p;
	}
	
	public void run()
	{
		try
		{
			System.out.println("Connecting to " + host + ", at port: " + port);
			Socket client = new Socket(host, port);
			System.out.println("Connected to " + client.getRemoteSocketAddress());
			
			DataInputStream in = new DataInputStream(client.getInputStream());
			NetworkRead r = new NetworkRead(in);
			r.setKey(e, c, d, dc);
			r.start();
			//Thread read = r;
			//read.start();
			
			DataOutputStream out = new DataOutputStream(client.getOutputStream());
			NetworkWrite w = new NetworkWrite(out);
			w.setKey(e, c, d, dc);
			w.start();
			//Thread write = new NetworkWrite(out);
			//write.start();
		}
		catch(IOException e)
		{
			e.printStackTrace();
			System.exit(1);
		}
	}
	
	public static void main(String[] args)
	{
		Client client = new Client(args[0], Integer.parseInt(args[1]));
		client.setKey(Long.parseLong(args[2]), Long.parseLong(args[3]), Long.parseLong(args[4]), Long.parseLong(args[5]));
		client.run();		
	}
}
