//package chat;

import java.util.Random;
import java.math.BigInteger;

public class MiniRSA {
	private static final int BOUND = 1000;
	public static long GCD(long a, long b)
	{
		if (b == 0)
			return a;
		if (a < b)
			return GCD(b, a);
		return GCD(b, a % b);
	}
	
	public static long coprime(long x)
	{
		Random rn = new Random();
		long res = 0;
		long check = 1000;
		while(check != 1)
		{
			res = rn.nextLong();
			check = GCD(res, x);
		}
		return res;
	}
	
	public static long modulo(long a, long b, long c)
	{
		long run = a % c;
		long res = 1;
		for (long i = 0; i < b; i++)
			res = (res * run) % c;
		return res;
	}
	
	public static long mod_inverse(long base, long m)
	{
		return BigInteger.valueOf((int) base).modInverse(BigInteger.valueOf((int) m)).intValue(); 
	}
	
	//https://en.wikipedia.org/wiki/Euler%27s_totient_function#The_function_.CF.86.28n.29_is_multiplicative
	public static long totient(long n)
	{
		long res = n;
		long run = n;
		for (long i = 2; i <= run / 2; i++)
		{
			if (run % i == 0)
			{
				res = res * (i - 1) / i;
				while (run % i == 0)
					run = run / i;
			}
		}
		return res;
	}
	
	public static boolean isPrime(long n)
	{
		for (long i = 2; i <= Math.sqrt(n) + 5; i++)
			if (n % i == 0)
				return false;
		return true;
	}
	
	//0: encrypt, 1: decrypt
	public static long encrypt(long ascii, long e, long c)
	{
		return modulo(ascii, e, c);
	}
	
	public static long decrypt(long ascii, long d, long c)
	{
		return modulo(ascii, d, c);
	}
	
	//return 3 numbers: (e, c, d)
	public static long[] randomKey()
	{
		Random rn = new Random();
		long d = 1, c = 0, e = 0;
		while(d == 1)
		{
			long a = 1000, b = 1000;
			while(a < 2 || !isPrime(a))
				a = Integer.toUnsignedLong(rn.nextInt(BOUND));
			while(b < 2 || !isPrime(b) || b == a)
				b = Integer.toUnsignedLong(rn.nextInt(BOUND));
			c = a * b;
			long m = (a - 1) * (b - 1);
			e = m * 2;
			while(GCD(e, m) != 1 || GCD(e, c) != 1 || isPrime(e))
				e++;
			d = mod_inverse(e, m);
		}
		long[] res = new long[3];
		res[0] = e;
		res[1] = c;
		res[2] = d;
		return res;
	}
	
	//given e and c, return d
	public static long crackKey(long e, long c)
	{
		//factor c
		long a = 0, b = 0;
		if (c % 2 == 0 && isPrime(c / 2))
		{
			a = 2;
			b = c / 2;
		}
		else
		{
			for (long run = 3; run <= Math.sqrt(c) + 5; run = run + 2)
			if (isPrime(run) && (c % run == 0) && isPrime(c / run))
			{
				a = run;
				b = c / run;
				break;
			}
		}
		long m = (a - 1) * (b - 1);
		e = m * 2;
		while(GCD(e, m) != 1 || GCD(e, c) != 1 || isPrime(e))
			e++;
		System.out.println("a: " + a + ", b: " + b + ", e: " + e + ", m: " + m);
		return mod_inverse(e, m);
	}
	
	public static long[] genKey(long a, long b)
	{
		long c = a * b;
		long m = (a - 1) * (b - 1);
		long e = m * 2;
		while(GCD(e, m) != 1 || GCD(e, c) != 1 || isPrime(e))
			e++;
		long d = mod_inverse(e, m);
		long[] res = new long[3];
		res[0] = e;
		res[1] = c;
		res[2] = d;
		return res;	
	}
	
	public static void main(String[] args)
	{
		System.out.println(isPrime(13107));
		long[] key = randomKey();
		key = randomKey();
		System.out.println("Randomly generate key. (e,c) is public key, (d, c) is private key, e: " + key[0] + ", c: " + key[1] + ", d: " + key[2]);
		char test = 'a';
		System.out.println("Test generating keys, encrypting and decoding 'a': ");
		long en = encrypt((long) test, key[0], key[1]);
		System.out.println("Encrypt 'a' to: " + en);
		System.out.println("Decrypt back to: " + (char) decrypt(en, key[2], key[1]));
		
		System.out.println(crackKey(1337, 65535));
		key = randomKey();
		System.out.println("Randomly generate key. (e,c) is public key, (d, c) is private key, e: " + key[0] + ", c: " + key[1] + ", d: " + key[2]);
		System.out.println("crack for d, given e and c: " + crackKey(key[0], key[1]));		
	}
}
