//package chat;

public class KeyGenTest {
	public static void main(String[] args)
	{
		long[] key = MiniRSA.genKey(Long.parseLong(args[0]), Long.parseLong(args[1]));
		System.out.println("Public key (" + key[0] + ", " + key[1] + "), private key (" + key[2] + ", " + key[1] + ")");
	}
}
