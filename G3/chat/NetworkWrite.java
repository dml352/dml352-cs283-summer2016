//package chat;

import java.io.*;
import java.util.*;

//read server message and send them to client
public class NetworkWrite extends Thread {
	private DataOutputStream out;
	private Scanner in;
	long e, c, d, dc;
	
	public void setKey(long first, long second, long third, long fourth)
	{
		e = first;
		c = second;
		d = third;
		dc = fourth;
	}
	
	public NetworkWrite(DataOutputStream o)
	{
		out = o;
		in = new Scanner(System.in);
	}
	
	public void run()
	{
		String buf = "";
		try
		{
			while(!buf.equals("quit"))
			{
				buf = in.nextLine();
				if (buf.equals("quit"))
				{
					System.out.println("Quitting the program");
					String end = "quit";
					out.writeUTF(end);
					System.exit(0);
				}
				for (int i = 0; i < buf.length(); i++)
				{
					String mes = "";
					mes = mes + MiniRSA.encrypt((long) buf.charAt(i), e, c);
					out.writeUTF(mes);
				}
				String mes = "" + MiniRSA.encrypt((long) '\n', e, c);
				out.writeUTF(mes);
			}
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
	}
}
