//package chat;

public class CrackTest {
	public static void main(String[] args)
	{
		long e = Long.parseLong(args[0]);
		long c = Long.parseLong(args[1]);
		long d = MiniRSA.crackKey(e, c);
		System.out.println(d);
		long en = MiniRSA.encrypt((long) 'a', e, c);
		System.out.println("Encrypt 'a': " + en);
		System.out.println("Decrypt back to: " + (char) MiniRSA.decrypt(en, d, c));
	}
}
