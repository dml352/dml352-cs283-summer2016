//package chat;

import java.net.*;
import java.io.*;

public class Server extends Thread {
	private ServerSocket serverSocket;
	long e, c, d, dc;

	public Server(int port) throws IOException
	{
		serverSocket = new ServerSocket(port);
		serverSocket.setReuseAddress(true);
	}
	
	public void setKey(long first, long second, long third, long fourth)
	{
		e = first;
		c = second;
		d = third;
		dc = fourth;
	}
	
	public void run()
	{
		try
		{
			System.out.println("Waiting for client to connect to port: " + serverSocket.getLocalPort() + "...");
			Socket server = serverSocket.accept();
			System.out.println("Connected to: " + server.getRemoteSocketAddress());
			System.out.println("Enter your message, or quit to end the program");
			
			DataInputStream in = new DataInputStream(server.getInputStream());
			NetworkRead r = new NetworkRead(in);
			r.setKey(e, c, d, dc);
			r.start();
			//Thread read = r;
			//read.start();
			
			DataOutputStream out = new DataOutputStream(server.getOutputStream());
			NetworkWrite w = new NetworkWrite(out);
			w.setKey(e, c, d, dc);
			w.start();
			//Thread write = new NetworkWrite(out);
			//write.start();
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args)
	{
		try
		{
			Server s = new Server(Integer.parseInt(args[0]));
			s.setKey(Long.parseLong(args[1]), Long.parseLong(args[2]), Long.parseLong(args[3]), Long.parseLong(args[4]));
			s.run();
		}
		catch(IOException e)
		{
			e.printStackTrace();
			System.exit(1);
		}
	}
}
