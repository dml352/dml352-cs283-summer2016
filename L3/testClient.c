#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <time.h> 
#include "csapp.h"

int main(int argc, char **argv)
{
	int clientfd, port;
	char *host, buf[MAXLINE];
	rio_t rio;
	host = argv[1]; port = atoi(argv[2]);
	clientfd = open_clientfd(host, port);
	rio_readinitb(&rio, clientfd);
	
	while(1)
	{
		fgets(buf, MAXLINE, stdin);
		rio_writen(clientfd, buf, strlen(buf));
	}
	
	close(clientfd);
	return 0;
}