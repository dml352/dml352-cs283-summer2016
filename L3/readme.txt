Name: Duc Le
Email: dml352@drexel.edu

Source:
http://csapp.cs.cmu.edu/2e/ics2/code/include/csapp.h
http://csapp.cs.cmu.edu/2e/ics2/code/src/csapp.c

client.c is the code for the client. 
Makefile targets:
build, for compiling the code.
clientTest, for testing the client. Running test with: make clientTest HOST="www.google.com" PAGE="/index.html"
The program will connect to port 80, get the page and print to the screen.

For the server part:
open another session in tux64-13.cs.drexel.edu, start a server with: make serverStart PORT=<port number>
In the original session, run: make testServer PORT=<port number>. Make sure the 2 port numbers are the same.
Enter the request in the form: GET /path HTTP/1.1\r\n\r\n, and the file will be sent from the server to the client.
The connection will continue, and you can send multiple requests, each will return a file. You can test with the 
files I submit (/client.c, /server.c, ...)


