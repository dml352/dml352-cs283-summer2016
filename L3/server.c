#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <time.h> 
#include "csapp.h"

void serveFile(int connfd)
{
	size_t n;
	char buf[MAXLINE];
	rio_t rio;
	
	rio_readinitb(&rio, connfd);
	while(1)
	{
		rio_readlineb(&rio, buf, MAXLINE);
		
		char *token = (char*) malloc(MAXLINE * sizeof(char));
		token = strtok(buf, " ");
		token = strtok(NULL, " ");
		
		char *cwd = (char*) malloc(MAXLINE * sizeof(char));
		cwd = getcwd(cwd, MAXLINE);
		
		strcat(cwd, token);
		FILE *readFile = fopen(cwd, "r");
		if (readFile == NULL)
		{
			printf("File not found.\n");
			strcpy(buf, "File not found.\n");
			rio_writen(connfd, buf, strlen(buf)); 
			return;
		}
		while (fgets(buf, MAXLINE, readFile) != NULL)
		{
			printf("%s", buf);
			rio_writen(connfd, buf, strlen(buf));
		}
	}
}

int main(int argc, char **argv)
{
	int listenfd, connfd, port, clientlen;
	struct sockaddr_in clientaddr;
	struct hostent *hp;
	char *haddrp;
	
	port = atoi(argv[1]);
	
	listenfd = open_listenfd(port);
	while(1)
	{
		clientlen = sizeof(clientaddr);
		connfd = accept(listenfd, (SA *)&clientaddr, &clientlen);
		hp = gethostbyaddr((const char*)&clientaddr.sin_addr.s_addr,
							sizeof(clientaddr.sin_addr.s_addr), AF_INET);
		haddrp = inet_ntoa(clientaddr.sin_addr);
		printf("Server connected to %s (%s)\n", hp -> h_name, haddrp);
		serveFile(connfd);
	}
	close(connfd);

	return 0;
}