#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <time.h> 
#include "csapp.h"

int main(int argc, char **argv)
{
	int clientfd, port;
	char *host, *page, buf[MAXLINE];
	rio_t rio;
	host = argv[1];
	page = argv[2];
	port = 80;
	clientfd = open_clientfd(host, port);
	rio_readinitb(&rio, clientfd);
	
	char *request = (char*) malloc(MAXLINE * sizeof(char));
	strcpy(request, "GET ");
	strcat(request, page);
	strcat(request, " HTTP/1.1\r\nHost: ");
	strcat(request, host);
	strcat(request, "\r\n\r\n");
	rio_writen(clientfd, request, strlen(request));
	char *read = (char*) malloc(MAXLINE * sizeof(char));
	int n;
	while((n = rio_readlineb(&rio, read, MAXLINE)) != 0)
	{
		if (n == -1)
		{
			printf("Error reading file\n");
			return -1;
		}
		printf("%s", read);
	}
	close(clientfd);
	
	return 0;
}

